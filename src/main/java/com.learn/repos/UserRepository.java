package com.learn.repos;
import com.learn.model.UploadFile;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<UploadFile, Integer> {
}
